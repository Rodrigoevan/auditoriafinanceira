package br.com.mastertech.producer;

import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, EmpresaResponse> producer;

    public void enviarAoKafka(EmpresaResponse empresa) {
        System.out.println("produzido envento empresa: " + empresa.getNome() + " capital social: " + empresa.getCapital_social());
        producer.send("spec2-rodrigo-carvalho-3", empresa);
    }
}
