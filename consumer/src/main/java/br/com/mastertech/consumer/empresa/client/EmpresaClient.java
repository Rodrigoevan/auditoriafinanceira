package br.com.mastertech.consumer.empresa.client;

import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "governo", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface EmpresaClient {
    @GetMapping("/{cnpj}")
    public EmpresaResponse getByCnpj(@PathVariable String cnpj);

}
