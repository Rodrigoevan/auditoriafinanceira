package br.com.mastertech.consumer.empresa.model.mapper;

import br.com.mastertech.consumer.empresa.client.EmpresaClient;
import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import br.com.mastertech.producer.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmpresaMapper {

    @Autowired
    EmpresaClient empresaClient;

    @Autowired
    EmpresaProducer empresaProducer;

    public EmpresaResponse getEmpresaGoverno(String cnpj){
        EmpresaResponse empresaResponse = empresaClient.getByCnpj(cnpj);
        System.out.println("capital social: " + empresaResponse.getCapital_social());
        empresaResponse.setCnpj(cnpj);
        empresaProducer.enviarAoKafka(empresaResponse);
        return empresaResponse;
    }
}
