package br.com.mastertech.consumer;

import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import br.com.mastertech.empresa.model.Empresa;
import br.com.mastertech.consumer.empresa.model.mapper.EmpresaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    protected EmpresaMapper empresaMapper;

    @KafkaListener(topics = "spec2-rodrigo-carvalho-2", groupId = "rocarev-1")
    public void receber(@Payload Empresa empresa){
        System.out.println("Recebi dados da empresa com CNPJ: " + empresa.getCnpj());
        EmpresaResponse empresaResponse = empresaMapper.getEmpresaGoverno(empresa.getCnpj());

    }
}
