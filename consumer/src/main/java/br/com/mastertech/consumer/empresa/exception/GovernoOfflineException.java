package br.com.mastertech.consumer.empresa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Microservico de busca de dados do CNPJ não está disponivel")
public class GovernoOfflineException extends RuntimeException {
}
