package br.com.mastertech.empresa.controller;

import br.com.mastertech.empresa.model.Empresa;
import br.com.mastertech.empresa.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmpresaController {

    @Autowired
    EmpresaService empresaService;

    @PostMapping
    public Empresa create(@RequestBody Empresa empresa){
        return empresaService.create(empresa);
    }

    @GetMapping("/{id}")
    public Empresa getById(@PathVariable Long id){
        return empresaService.getById(id);
    }
    @GetMapping("/cnpj/{CNPJ}")
    public Empresa getByCNPJ(String CNPJ){
        return empresaService.getByCNPJ(CNPJ);
    }
}
