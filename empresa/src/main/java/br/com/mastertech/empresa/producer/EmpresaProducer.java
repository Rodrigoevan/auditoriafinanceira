package br.com.mastertech.empresa.producer;

import br.com.mastertech.empresa.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {
    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public void enviarAoKafka(Empresa empresa) {
        System.out.println("produzido envento empresa: " + empresa.getNome());
        producer.send("spec2-rodrigo-carvalho-2", empresa);
    }
}
