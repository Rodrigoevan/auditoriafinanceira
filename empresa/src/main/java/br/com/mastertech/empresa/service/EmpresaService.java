package br.com.mastertech.empresa.service;

import br.com.mastertech.empresa.model.Empresa;
import br.com.mastertech.empresa.model.StatusReceita;
import br.com.mastertech.empresa.producer.EmpresaProducer;
import br.com.mastertech.empresa.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    EmpresaRepository empresaRepository;
    @Autowired
    EmpresaProducer empresaProducer;

    public Empresa create(Empresa empresa){
        empresa.setStatusReceita(StatusReceita.PENDENTE);
        Empresa empresaObjeto = empresaRepository.save(empresa);
        empresaProducer.enviarAoKafka(empresaObjeto);
        return empresaObjeto;
    }

    public Empresa getById(Long id) {
        return empresaRepository.findById(id).get();
    }

    public Empresa getByCNPJ(String cnpj) {
        return empresaRepository.findByCnpj(cnpj).get();
    }
}
