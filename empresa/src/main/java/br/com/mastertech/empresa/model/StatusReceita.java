package br.com.mastertech.empresa.model;

public enum StatusReceita {
    PENDENTE,
    APROVADO,
    NEGADO
}
