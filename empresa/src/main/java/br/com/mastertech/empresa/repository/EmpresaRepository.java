package br.com.mastertech.empresa.repository;

import br.com.mastertech.empresa.model.Empresa;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmpresaRepository extends CrudRepository<Empresa, Long> {
    Optional<Empresa> findByCnpj(String cnpj);
}
