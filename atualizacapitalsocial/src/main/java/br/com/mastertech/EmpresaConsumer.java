package br.com.mastertech;

import br.com.mastertech.atualizacapitalsocial.service.EmpresaService;
import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class EmpresaConsumer {

    @Autowired
    EmpresaService empresaService;

    @KafkaListener(topics = "spec2-rodrigo-carvalho-3", groupId = "rocarev-1")
    public void receber(@Payload EmpresaResponse empresaResponse) throws IOException {
        System.out.println("recebi dados em empresa atualizado" + empresaResponse.getCnpj());
        empresaService.update(empresaResponse);

    }

}
