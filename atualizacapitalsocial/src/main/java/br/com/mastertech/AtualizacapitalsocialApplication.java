package br.com.mastertech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtualizacapitalsocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtualizacapitalsocialApplication.class, args);
	}

}
