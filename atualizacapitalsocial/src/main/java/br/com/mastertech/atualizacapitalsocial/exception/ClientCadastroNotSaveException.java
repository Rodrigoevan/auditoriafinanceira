package br.com.mastertech.atualizacapitalsocial.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;

@ResponseStatus(code = HttpStatus.NOT_MODIFIED, reason = "Empresa não foi salva")
public class ClientCadastroNotSaveException extends IOException {
}
