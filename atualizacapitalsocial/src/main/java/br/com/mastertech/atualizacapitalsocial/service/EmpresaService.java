package br.com.mastertech.atualizacapitalsocial.service;

import br.com.mastertech.atualizacapitalsocial.exception.ClientCadastroNotSaveException;
import br.com.mastertech.atualizacapitalsocial.model.Empresa;
import br.com.mastertech.consumer.empresa.client.dto.EmpresaResponse;
import br.com.mastertech.atualizacapitalsocial.model.StatusReceita;
import br.com.mastertech.atualizacapitalsocial.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

@Service
public class EmpresaService {

    @Autowired
    EmpresaRepository empresaRepository;

    public Empresa update(EmpresaResponse empresaResponse) throws IOException {
        Optional<Empresa> empresaOptional = empresaRepository.findByCnpj(empresaResponse.getCnpj());

        Empresa empresa = empresaOptional.get();
        empresa.setValor(empresaResponse.getCapital_social());
        if(empresa.getValor() > 1000000.00){
            empresa.setStatusReceita(StatusReceita.APROVADO);
        }else {
            empresa.setStatusReceita(StatusReceita.NEGADO);
        }

        Empresa empresaObjeto = empresaRepository.save(empresa);
        gravarArquivoCadastro(empresa);
        return empresaObjeto;
    }

    public Empresa getById(Long id) {
        return empresaRepository.findById(id).get();
    }

    public Empresa getByCNPJ(String cnpj) {
        return empresaRepository.findByCnpj(cnpj).get();
    }

    public void gravarArquivoCadastro (Empresa empresa) throws IOException {

        try {
            FileWriter fw = new FileWriter("cadastro.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);


            pw.println(empresa.getNome() + "," + empresa.getCnpj()+ "," + empresa.getValor() + "," + empresa.getStatusReceita());
            pw.flush();
            pw.close();
        }catch(Exception E) {
            throw new ClientCadastroNotSaveException();
        }
    }

}
