package br.com.mastertech.atualizacapitalsocial.model;

public enum StatusReceita {
    PENDENTE,
    APROVADO,
    NEGADO
}
