package br.com.mastertech.atualizacapitalsocial.repository;

import br.com.mastertech.atualizacapitalsocial.model.Empresa;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmpresaRepository extends CrudRepository<Empresa, Long> {
    Optional<Empresa> findByCnpj(String cnpj);
}
